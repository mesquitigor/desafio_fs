package worshop;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Teste {

	
	@Test
	public void fluxoFeliz() {
		WebDriverManager.chromiumdriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
		WebElement usernameBox = driver.findElement(By.name("username"));
		WebElement passwordBox = driver.findElement(By.name("password"));
		WebElement commentsBox = driver.findElement(By.name("comments"));
		
		usernameBox.sendKeys("QA User");
		passwordBox.sendKeys("QA Password");
		commentsBox.clear();
		commentsBox.sendKeys("Comentario");
		
		WebElement checkbox1 = driver.findElement(By.xpath("//input[@value='cb1']"));
		WebElement checkbox2 = driver.findElement(By.xpath("//input[@value='cb2']"));
			checkbox1.click();
			checkbox2.click();
	
		WebElement radioitem = driver.findElement(By.xpath("//input[@value='rd1']"));
			radioitem.click();

		WebElement multipleselect4 = driver.findElement(By.name("multipleselect[]"));
			Select selectmultipletselect4 = new Select(multipleselect4);
					selectmultipletselect4.deselectAll();
			
		WebElement multipleselect1 = driver.findElement(By.name("multipleselect[]"));
			Select selectmultipletselect1 = new Select(multipleselect1);		
					selectmultipletselect1.selectByValue("ms1");
				
		WebElement dropdown = driver.findElement(By.name("dropdown"));
			Select selectdropdown = new Select(dropdown);
					selectdropdown.selectByValue("dd1");
		
		WebElement submit = driver.findElement(By.xpath("//input[@value='submit']"));
			submit.click();
			
		Assert.assertEquals("QA User", driver.findElement(By.id("_valueusername")).getText());
		Assert.assertEquals("QA Password", driver.findElement(By.id("_valuepassword")).getText());
		Assert.assertEquals("Comentario", driver.findElement(By.id("_valuecomments")).getText());
		Assert.assertEquals("cb1", driver.findElement(By.id("_valuecheckboxes0")).getText());
		Assert.assertEquals("cb2", driver.findElement(By.id("_valuecheckboxes1")).getText());		
		Assert.assertEquals("cb3", driver.findElement(By.id("_valuecheckboxes2")).getText());	
		Assert.assertEquals("rd1", driver.findElement(By.id("_valueradioval")).getText());
		Assert.assertEquals("ms1", driver.findElement(By.id("_valuemultipleselect0")).getText());
		Assert.assertEquals("dd1", driver.findElement(By.id("_valuedropdown")).getText());
		Assert.assertEquals("submit", driver.findElement(By.id("_valuesubmitbutton")).getText());
		driver.quit();
		
		
	}
	
	@Test
	public void fluxoAlternativo() {
		WebDriverManager.chromiumdriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
		WebElement commentsBox = driver.findElement(By.name("comments"));
		commentsBox.clear();
		
		WebElement radioitem = driver.findElement(By.xpath("//input[@value='rd1']"));
		radioitem.click();
		
		WebElement multipleselect4 = driver.findElement(By.name("multipleselect[]"));
		Select selectmultipletselect4 = new Select(multipleselect4);
				selectmultipletselect4.deselectAll();
		
		WebElement multipleselect1 = driver.findElement(By.name("multipleselect[]"));
		Select selectmultipletselect1 = new Select(multipleselect1);		
				selectmultipletselect1.selectByValue("ms1");
				
		WebElement dropdown = driver.findElement(By.name("dropdown"));
		Select selectdropdown = new Select(dropdown);
				selectdropdown.selectByValue("dd1");
		
		WebElement submit = driver.findElement(By.xpath("//input[@value='submit']"));
			submit.click();
			
		Assert.assertEquals("No Value for username", driver.findElement(By.xpath("//body/div/div[3]/p[1]/strong")).getText());
		Assert.assertEquals("No Value for password", driver.findElement(By.xpath("//body/div/div[3]/p[2]/strong")).getText());
		Assert.assertEquals("No Value for comments", driver.findElement(By.xpath("//body/div/div[3]/p[3]/strong")).getText());
		Assert.assertEquals("cb3", driver.findElement(By.id("_valuecheckboxes0")).getText());	
		Assert.assertEquals("rd1", driver.findElement(By.id("_valueradioval")).getText());
		Assert.assertEquals("ms1", driver.findElement(By.id("_valuemultipleselect0")).getText());
		Assert.assertEquals("dd1", driver.findElement(By.id("_valuedropdown")).getText());
		Assert.assertEquals("submit", driver.findElement(By.id("_valuesubmitbutton")).getText());
		driver.quit(); 
	}
	
}